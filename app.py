from flask import Flask, render_template, redirect, url_for, request
import seq2seqmodel as sq

app = Flask(__name__)
#app.config['ENV'] = 'production'
@app.route('/')
def landing_page():
    return render_template("landingpage.html")



@app.route('/translator',methods = ['POST','GET'])
def translator():
    return render_template("translator.html")
#     if request.method == "POST":
#         english = request.form['english']
#         return render_template("translatoranswer.html", english=english)
#     else:
#         return redirect(url_for('translator'))


@app.route('/translatenow', methods = ['POST', 'GET'])
def translatenow():
    if request.method == "POST":
        english = request.form['english']
        english = english.lower()
        try:
            
           french_with_eos=sq.translate(english)
           french=french_with_eos.strip('<EOS>')
            
        except KeyError:
           print('word not in vocab')
           return render_template("errorpage.html")
        
        return render_template("translatoranswer.html", english=english, french=french)



if __name__ == '__main__':
    app.run()
