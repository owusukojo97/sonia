# SONIA

A simple English to French translator 
using encoder-decoder RNN with attention in pytorch.

find dataset in data/eng-fra.txt

run flaskapp using app.py
this will begin training which, depending on your computer can take a while

Its trained with smaller subset of data i.e short phrases which begin with 
"i am, he is, she is," etc

seq2seqmodel.py contains the model 